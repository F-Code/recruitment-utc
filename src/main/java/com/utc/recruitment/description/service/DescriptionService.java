package com.utc.recruitment.description.service;

import javax.transaction.Transactional;

import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utc.recruitment.description.bean.DescriptionBean;
import com.utc.recruitment.description.form.DescriptionForm;
import com.utc.recruitment.description.bo.DescriptionBO;
import com.utc.recruitment.description.dao.DescriptionDAO;


@Service
public class DescriptionService {

    @Autowired
    private DescriptionDAO descriptiondao;
    @Autowired
    private CommonService commonService;

    /**
     * findById
     * @param DescriptionId
     * @return
     */
    public DescriptionBO findById(Long DescriptionId) {
        return descriptiondao.findById(DescriptionId).orElse(null);
    }

    /**
     * getDatatables
     * @param delineationForm
     * @return
     */
    public DataTableResults<DescriptionBean> getDatatables(DescriptionForm delineationForm) {
        return descriptiondao.getDatatables(commonService, delineationForm);
    }

    /**
     * saveOrUpdate
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(DescriptionBO entity) {
        descriptiondao.save(entity);
    }

    /**
     * delete
     * @param entity
     */
    public void delete(DescriptionBO entity) {
        descriptiondao.delete(entity);
    }
}
