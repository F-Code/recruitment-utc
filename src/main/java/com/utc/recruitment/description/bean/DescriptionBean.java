
package com.utc.recruitment.description.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DescriptionBean for table "descriptions"
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DescriptionBean {

    private Long id;
    private String content;
    private String name;
}
