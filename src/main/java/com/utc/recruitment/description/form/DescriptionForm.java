package com.utc.recruitment.description.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DescriptionForm {

    private Long id;
    private String content;
    private String name;
    private Integer page;
    private Integer recordPage;
}
