

package com.utc.recruitment.description.dao;

import com.utc.recruitment.description.bean.DescriptionBean;
import com.utc.recruitment.description.bo.DescriptionBO;
import com.utc.recruitment.description.form.DescriptionForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public interface DescriptionDAO extends CrudRepository<DescriptionBO, Long> {
    /**
     * List all Description
     */
    public List<DescriptionBO> findAll();

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<DescriptionBean> getDatatables(CommonService commonService, DescriptionForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,content As content      ";
        sql += "       ,name As name         ";
        sql += "       FROM descriptions ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");


        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getContent(), strCondition, paramList, "content");
        Mixin.filter(formData.getName(), strCondition, paramList, "name");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, DescriptionBean.class, formData.getPage(), formData.getRecordPage());
    }
}
