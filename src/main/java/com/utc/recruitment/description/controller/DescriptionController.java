
package com.utc.recruitment.description.controller;

import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.description.bean.DescriptionBean;
import com.utc.recruitment.description.form.DescriptionForm;
import com.utc.recruitment.description.bo.DescriptionBO;
import com.utc.recruitment.description.service.DescriptionService;


@Controller
@RequestMapping("/api/descriptions")
@PreAuthorize("hasAnyRole('Marketing', 'Admin', 'HR', 'HR Admin', 'Candidate', 'Visiter')")
public class DescriptionController {

    @Autowired
    private DescriptionService descriptionService;

    /**
     * findById
     * @param descriptionId
     * @return
     */
    @GetMapping(path = "/{descriptionId}")
    public @ResponseBody
    Response findById( @PathVariable Long descriptionId) {

        DescriptionBO delineationBO = descriptionService.findById(descriptionId);
        if(delineationBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(delineationBO);
    }

    /**
     * processSearch
     * @param form
     * @return DataTableResults
     */
    @GetMapping(path = "/get-all")
    public @ResponseBody
    DataTableResults<DescriptionBean> processSearch(DescriptionForm form) {

        return descriptionService.getDatatables(form);
    }

    /**
     * saveOrUpdate DescriptionBO
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody Response saveOrUpdate( @RequestBody DescriptionForm form) throws Exception {
        Long id = Mixin.NVL(form.getId());
        DescriptionBO delineationBO;
        if(id > 0L) {

            delineationBO = descriptionService.findById(id);
            if(delineationBO == null){
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {

            delineationBO = new DescriptionBO();
        }
        delineationBO.setContent(form.getContent());
        delineationBO.setName(form.getName());
        descriptionService.saveOrUpdate(delineationBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(delineationBO);
    }

    /**
     * delete
     * @param descriptionId
     * @return
     */
    @DeleteMapping(path = "/{descriptionId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Response delete( @PathVariable Long descriptionId) {

        DescriptionBO bo ;
        if(descriptionId > 0L) {
            bo = descriptionService.findById(descriptionId);
            if(bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            descriptionService.delete(bo);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }
}
