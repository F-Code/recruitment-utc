

package com.utc.recruitment.infomation.service;

import com.utc.recruitment.infomation.bean.InfomationBean;
import com.utc.recruitment.infomation.bo.InfomationBO;
import com.utc.recruitment.infomation.dao.InfomationDAO;
import com.utc.recruitment.infomation.form.InfomationForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class InfomationService {

    @Autowired
    private InfomationDAO infomationdao;
    @Autowired
    private CommonService commonService;

    /**
     * findById
     *
     * @param InfomationId
     * @return
     */
    public InfomationBO findById(Long InfomationId) {
        return infomationdao.findById(InfomationId).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param infomationForm
     * @return
     */
    public DataTableResults<InfomationBean> getDatatables(InfomationForm infomationForm) {
        return infomationdao.getDatatables(commonService, infomationForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(InfomationBO entity) {
        infomationdao.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(InfomationBO entity) {
        infomationdao.delete(entity);
    }
}
