
package com.utc.recruitment.infomation.controller;

import com.utc.recruitment.infomation.bean.InfomationBean;
import com.utc.recruitment.infomation.bo.InfomationBO;
import com.utc.recruitment.infomation.form.InfomationForm;
import com.utc.recruitment.infomation.service.InfomationService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author d2tsoftware
 * @version 1.0
 * @since 17/04/2021
 */
@Controller
@RequestMapping("/api/infomation")
public class InfomationController {

    @Autowired
    private InfomationService infomationService;

    /**
     * findById
     *
     * @param infomationId
     * @return
     */
    @GetMapping(path = "/{infomationId}")
    public @ResponseBody
    Response findById(@PathVariable Long infomationId) {

        InfomationBO infomationBO = infomationService.findById(infomationId);
        if (infomationBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(infomationBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @GetMapping(path = "/get-all")
    public @ResponseBody
    DataTableResults<InfomationBean> processSearch(InfomationForm form) {
        return infomationService.getDatatables(form);
    }

    /**
     * saveOrUpdate InfomationBO
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody InfomationForm form) throws Exception {
        Long id = Mixin.NVL(form.getId());
        InfomationBO infomationBO;
        if (id > 0L) {

            infomationBO = infomationService.findById(id);
            if (infomationBO == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {

            infomationBO = new InfomationBO();
        }
        infomationBO.setDescription(form.getDescription());
        infomationBO.setEmail(form.getEmail());
        infomationBO.setFax(form.getFax());
        infomationBO.setLevel(form.getLevel());
        infomationBO.setLogoPath(form.getLogoPath());
        infomationBO.setName(form.getName());
        infomationBO.setPhone(form.getPhone());
        infomationBO.setAddress(form.getAddress());
        infomationService.saveOrUpdate(infomationBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(infomationBO);
    }

    /**
     * delete
     *
     * @param infomationId
     * @return
     */
    @DeleteMapping(path = "/{infomationId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long infomationId) {

        InfomationBO bo;
        if (infomationId > 0L) {
            bo = infomationService.findById(infomationId);
            if (bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            infomationService.delete(bo);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }
}
