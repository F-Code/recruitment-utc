
package com.utc.recruitment.infomation.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * InfomationForm class
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
public class InfomationForm {

    private Long id;
    private String description;
    private String email;
    private String fax;
    private Integer level;
    private String logoPath;
    private String name;
    private String phone;
    private String address;
    private Integer page;
    private Integer recordPage;
}
