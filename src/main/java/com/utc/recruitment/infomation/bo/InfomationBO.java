

package com.utc.recruitment.infomation.bo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "infomations")
public class InfomationBO {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String description;

    @Column
    private String email;

    @Column
    private String fax;

    @Column
    private Integer level;

    @Column
    private String logoPath;

    @Column
    private String name;

    @Column
    private String phone;

    @Column
    private String address;
}
