
package com.utc.recruitment.infomation.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * InfomationBean for table "infomations"
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InfomationBean {

    private Long id;
    private String description;
    private String email;
    private String fax;
    private Integer level;
    private String logoPath;
    private String name;
    private String phone;
    private String address;
}
