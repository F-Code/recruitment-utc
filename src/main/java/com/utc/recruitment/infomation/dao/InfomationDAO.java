package com.utc.recruitment.infomation.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.infomation.bean.InfomationBean;
import com.utc.recruitment.infomation.form.InfomationForm;
import com.utc.recruitment.infomation.bo.InfomationBO;


@Transactional
@Repository
public interface InfomationDAO extends CrudRepository<InfomationBO, Long> {
    /**
     * List all Infomation
     */
    public List<InfomationBO> findAll();

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<InfomationBean> getDatatables(CommonService commonService, InfomationForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,description As description  ";
        sql += "       ,email As email        ";
        sql += "       ,fax As fax          ";
        sql += "       ,level As level        ";
        sql += "       ,logo_path As logoPath     ";
        sql += "       ,name As name         ";
        sql += "       ,phone As phone        ";
        sql += "       ,address As address      ";
        sql += "       FROM infomations ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");


        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getDescription(), strCondition, paramList, "description");
        Mixin.filter(formData.getEmail(), strCondition, paramList, "email");
        Mixin.filter(formData.getFax(), strCondition, paramList, "fax");
        Mixin.filter(formData.getLevel(), strCondition, paramList, "level");
        Mixin.filter(formData.getLogoPath(), strCondition, paramList, "logo_path");
        Mixin.filter(formData.getName(), strCondition, paramList, "name");
        Mixin.filter(formData.getPhone(), strCondition, paramList, "phone");
        Mixin.filter(formData.getAddress(), strCondition, paramList, "address");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, InfomationBean.class, formData.getPage(), formData.getRecordPage());
    }
}
