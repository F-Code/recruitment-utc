
package com.utc.recruitment.support.controller;

import com.utc.recruitment.mail.MailService;
import com.utc.recruitment.support.bean.SupportBean;
import com.utc.recruitment.support.bo.SupportBO;
import com.utc.recruitment.support.form.SupportForm;
import com.utc.recruitment.support.form.SupportReplyEmailForm;
import com.utc.recruitment.support.service.SupportService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/support")
// Lấy chi tiết user
@PreAuthorize("hasAnyRole('Marketing', 'Admin', 'HR', 'HR Admin', 'Candidate', 'Visiter')")
public class SupportController {

    @Autowired
    private SupportService supportService;

    @Autowired
    private MailService mailService;

    /**
     * findById
     *
     * @param supportId
     * @return
     */
    @GetMapping(path = "/{supportId}")
    public @ResponseBody
    Response findById(@PathVariable Long supportId) {

        SupportBO supportBO = supportService.findById(supportId);
        if (supportBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(supportBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @GetMapping(path = "/get-all")
    public @ResponseBody
    DataTableResults<SupportBean> processSearch(SupportForm form) {
        return supportService.getDatatables(form);
    }

    /**
     * saveOrUpdate SupportBO
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody SupportForm form) throws Exception {
        Long id = Mixin.NVL(form.getId());
        SupportBO supportBO;
        if (id > 0L) {

            supportBO = supportService.findById(id);
            if (supportBO == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {
            supportBO = new SupportBO();
        }
        supportBO.setContent(form.getContent());
        supportBO.setEmail(form.getEmail());
        supportBO.setPhoneNumber(form.getPhoneNumber());
        supportBO.setStatus(form.getStatus() != null ? form.getStatus() : Constants.SUPPORT.IS_NOT_SUPPORTED);
        supportBO.setTitle(form.getTitle());
        supportService.saveOrUpdate(supportBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(supportBO);
    }

    /**
     * delete
     *
     * @param supportId
     * @return
     */
    @DeleteMapping(path = "/{supportId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long supportId) {

        SupportBO bo;
        if (supportId > 0L) {
            bo = supportService.findById(supportId);
            if (bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            supportService.delete(bo);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }

    @PostMapping(path = "/send-email")
    public Response saveUser(@RequestBody SupportReplyEmailForm form) {
        //Lấy bản ghi support
        SupportBO supportBO = supportService.findById(form.getId());
        // Validate email
        if (supportBO.getEmail() != null && !supportBO.getEmail().trim().equals("")) {
            String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
            if (!regex.matches(supportBO.getEmail())) {
                return Response.error("Email " + supportBO.getEmail() + " không hợp lệ");
            }
        }

        // Cập nhật trạng thái đã gửi mail support
        supportBO.setStatus(Constants.SUPPORT.IS_SUPPORTED);
        supportService.saveOrUpdate(supportBO);
        mailService.sendEmail(supportBO.getEmail(), form.getReplyContent(), "Econet: Hỗ trợ khách hàng theo yêu cầu số: " + form.getId());

        return Response.success("Đã gửi email support khách hàng");
    }
}
