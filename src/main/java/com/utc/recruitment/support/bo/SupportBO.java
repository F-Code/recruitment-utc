

package com.utc.recruitment.support.bo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "supports")
public class SupportBO {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Nội dung yêu cầu support
    @Column(name = "content")
    private String content;

    // email người gửi support
    @Column(name = "email")
    private String email;

    // Số điện thoại
    @Column(name = "phone_number")
    private String phoneNumber;

    // Trạng thái support
    @Column(name = "status")
    private Integer status;

    // Tiêu đề support
    @Column(name = "title")
    private String title;

}
