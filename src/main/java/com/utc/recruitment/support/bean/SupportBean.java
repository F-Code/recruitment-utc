
package com.utc.recruitment.support.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * SupportBean for table "supports"
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupportBean {

    private Long id;
    private String content;
    private String email;
    private String phoneNumber;
    private Integer status;
    private String title;
}
