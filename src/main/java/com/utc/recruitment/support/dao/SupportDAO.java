

package com.utc.recruitment.support.dao;

import com.utc.recruitment.support.bean.SupportBean;
import com.utc.recruitment.support.bo.SupportBO;
import com.utc.recruitment.support.form.SupportForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Transactional
@Repository
public interface SupportDAO extends CrudRepository<SupportBO, Long> {
    /**
     * List all Support
     */
    public List<SupportBO> findAll();

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<SupportBean> getDatatables(CommonService commonService, SupportForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,content As content      ";
        sql += "       ,email As email        ";
        sql += "       ,phone_number As phoneNumber  ";
        sql += "       ,status As status       ";
        sql += "       ,title As title        ";
        sql += "       FROM supports ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");


        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getContent(), strCondition, paramList, "content");
        Mixin.filter(formData.getEmail(), strCondition, paramList, "email");
        Mixin.filter(formData.getPhoneNumber(), strCondition, paramList, "phone_number");
        Mixin.filter(formData.getStatus(), strCondition, paramList, "status");
        Mixin.filter(formData.getTitle(), strCondition, paramList, "title");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, SupportBean.class, formData.getPage(), formData.getRecordPage());
    }
}
