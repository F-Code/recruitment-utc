
package com.utc.recruitment.support.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
/**
 * SupportForm class
 *
 * @author
 * @since 1.0
 * @version 1.0
 */
public class SupportForm {

    private Long id;
    private String content;
    private String email;
    private String phoneNumber;
    private Integer status;
    private String title;
    private Integer page;
    private Integer recordPage;
}
