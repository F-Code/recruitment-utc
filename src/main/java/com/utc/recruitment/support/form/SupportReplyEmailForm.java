package com.utc.recruitment.support.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SupportReplyEmailForm {
    // id bản ghi gửi mail
    private Long id;
    // Nội dung phản hồi khách
    private String replyContent;
}
