

package com.utc.recruitment.support.service;

import javax.transaction.Transactional;

import com.utc.recruitment.support.bo.SupportBO;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.utc.recruitment.support.bean.SupportBean;
import com.utc.recruitment.support.form.SupportForm;
import com.utc.recruitment.support.dao.SupportDAO;


@Service
public class SupportService {

    @Autowired
    private SupportDAO supportdao;
    @Autowired
    private CommonService commonService;

    /**
     * findById
     * @param SupportId
     * @return
     */
    public SupportBO findById(Long SupportId) {
        return supportdao.findById(SupportId).orElse(null);
    }

    /**
     * getDatatables
     * @param supportForm
     * @return
     */
    public DataTableResults<SupportBean> getDatatables(SupportForm supportForm) {
        return supportdao.getDatatables(commonService, supportForm);
    }

    /**
     * saveOrUpdate
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(SupportBO entity) {
        supportdao.save(entity);
    }

    /**
     * delete
     * @param entity
     */
    public void delete(SupportBO entity) {
        supportdao.delete(entity);
    }
}
