package com.utc.recruitment.navbar.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavbarForm {
    private Long id;
    private String description;
    private Integer level;
    private String name;
    private String url;
    private Integer page;
    private Integer recordPage;
    private Integer type;
    private Long parentId;
}
