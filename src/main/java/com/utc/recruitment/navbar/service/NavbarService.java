

package com.utc.recruitment.navbar.service;

import com.utc.recruitment.navbar.bean.DataNavbarBean;
import com.utc.recruitment.navbar.bean.NavbarBean;
import com.utc.recruitment.navbar.bo.NavbarBO;
import com.utc.recruitment.navbar.dao.NavbarDAO;
import com.utc.recruitment.navbar.form.NavbarForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class NavbarService {

    @Autowired
    private NavbarDAO navbardao;
    @Autowired
    private CommonService commonService;

    /**
     * findById
     *
     * @param NavbarId
     * @return
     */
    public NavbarBO findById(Long NavbarId) {
        return navbardao.findById(NavbarId).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param navbarForm
     * @return
     */
    public DataTableResults<NavbarBean> getDatatables(NavbarForm navbarForm) {
        return navbardao.getDatatables(commonService, navbarForm);
    }

    /**
     * Truyền type = 1 là dành cho trang admin và 0 là dành cho trang user
     *
     * @param navbarForm
     * @return
     */
    public List<DataNavbarBean> getMenuWithType(NavbarForm navbarForm) {
        // Gọi hàm lấy danh sách menu từ db
        List<NavbarBean> beans = navbardao.getMenuWithType(commonService, navbarForm);
        // Lọc xem là menu cấp 1 hay 2
        List<NavbarBean> beansLevel1 = beans.stream().filter(e -> e.getLevel() == 1).collect(Collectors.toList());
        List<NavbarBean> beansLevel2 = beans.stream().filter(e -> e.getLevel() == 2).collect(Collectors.toList());

        // Tạo 1 data kết quả trả về
        List<DataNavbarBean> dataNavbarBeans = new ArrayList<>();
        //Set giá trị cho các menu cấp 1
        for (NavbarBean bean : beansLevel1) {
            dataNavbarBeans.add(new DataNavbarBean(bean, null));
        }
        // Khời tạo 1 list danh sách menu con
        List<NavbarBean> listChildMenu = new ArrayList<>();

        // Thêm list menu con vào từng menu cha nếu có
        for (NavbarBean bean : beansLevel2) {
            for (DataNavbarBean dataNavbarBean : dataNavbarBeans) {
                if (bean.getParentId().equals(dataNavbarBean.getParentNavbar().getId())) {
                    listChildMenu.add(bean);
                    dataNavbarBean.setListChildNavbar(listChildMenu);
                }
            }
        }
        return dataNavbarBeans;
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(NavbarBO entity) {
        navbardao.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(NavbarBO entity) {
        navbardao.delete(entity);
    }
}
