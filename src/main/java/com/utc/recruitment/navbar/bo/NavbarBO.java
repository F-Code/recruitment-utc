package com.utc.recruitment.navbar.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "navbars")
public class NavbarBO {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Miêu tả
    @Column(name = "description")
    private String description;

    // Cấp độ
    @Column(name = "level")
    private Integer level;

    // Tên menu
    @Column(name = "name")
    private String name;

    // Đường dẫn đến page
    @Column(name = "url")
    private String url;

    // Id cha
    @Column(name = "parent_id")
    private Long parentId;

    // Đường dẫn đến page
    @Column(name = "type")
    private Integer type;
}
