package com.utc.recruitment.navbar.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataNavbarBean {
    private NavbarBean parentNavbar;

    private List<NavbarBean> listChildNavbar;
}
