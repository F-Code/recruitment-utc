
package com.utc.recruitment.navbar.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavbarBean {
    private Long id;
    private String description;
    private Integer level;
    private String name;
    private String url;
    private Integer type;
    private Long parentId;
}
