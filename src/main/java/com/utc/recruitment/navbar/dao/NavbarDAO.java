package com.utc.recruitment.navbar.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.utc.recruitment.navbar.bo.NavbarBO;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.navbar.bean.NavbarBean;
import com.utc.recruitment.navbar.form.NavbarForm;

@Transactional
@Repository
public interface NavbarDAO extends CrudRepository<NavbarBO, Long>
{
    /**
     * List all Navbar
     */
    public List<NavbarBO> findAll();

    /**
     * get data by datatable
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<NavbarBean> getDatatables(CommonService commonService, NavbarForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,description As description  ";
        sql += "       ,level As level        ";
        sql += "       ,name As name         ";
        sql += "       ,url As url          ";
        sql += "       ,parent_id As parentId          ";
        sql += "       ,type As type          ";
        sql += "       FROM navbars ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");


        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getDescription(), strCondition, paramList, "description");
        Mixin.filter(formData.getLevel(), strCondition, paramList, "level");
        Mixin.filter(formData.getName(), strCondition, paramList, "name");
        Mixin.filter(formData.getUrl(), strCondition, paramList, "url");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, NavbarBean.class, formData.getPage(), formData.getRecordPage());
    }

    /**
     * get data by datatable
     * @param commonService
     * @param formData
     * @return
     */
    default List<NavbarBean> getMenuWithType(CommonService commonService, NavbarForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,description As description  ";
        sql += "       ,level As level        ";
        sql += "       ,name As name         ";
        sql += "       ,url As url          ";
        sql += "       ,parent_id As parentId          ";
        sql += "       ,type As type          ";
        sql += "       FROM navbars ";
        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");
        if ((formData.getType() != null)) {
            strCondition.append(" AND ").append(" type ").append(" = " + formData.getType());
        }
        return commonService.list(sql + strCondition.toString(), paramList, NavbarBean.class);
    }
}
