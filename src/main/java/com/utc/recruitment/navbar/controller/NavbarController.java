package com.utc.recruitment.navbar.controller;

import com.utc.recruitment.navbar.bo.NavbarBO;
import com.utc.recruitment.navbar.form.NavbarForm;
import com.utc.recruitment.navbar.service.NavbarService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/api/navbar")
public class NavbarController {

    @Autowired
    private NavbarService navbarService;

    /**
     * findById
     *
     * @param navbarId
     * @return
     */
    @PreAuthorize("hasRole('Admin')")
    @GetMapping(path = "/{navbarId}")
    public @ResponseBody
    Response findById(@PathVariable Long navbarId) {

        NavbarBO navbarsBO = navbarService.findById(navbarId);
        if (navbarsBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(navbarsBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @PreAuthorize("hasRole('Admin')")
    @PostMapping(path = "/get-all")
    public @ResponseBody
    Response processSearch(@RequestBody NavbarForm form) {
        return Response.success("Lấy danh sách thành công")
                .withData(navbarService.getDatatables(form));
    }

    /**
     * Truyền type = 1 là dành cho trang admin và 0 là dành cho trang user
     *
     * @param form
     * @return DataTableResults
     */
    @PreAuthorize("hasRole('Admin')")
    @PostMapping(path = "/get-menus")
    public @ResponseBody
    Response getMenuWithType(@RequestBody NavbarForm form) {
        return Response.success("Lấy danh sách thành công")
                .withData(navbarService.getMenuWithType(form));
    }

    /**
     * saveOrUpdate NavbarBO
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PreAuthorize("hasRole('Admin')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody NavbarForm form) {
        Long id = Mixin.NVL(form.getId());
        NavbarBO navbarBO;
        if (id > 0L) {

            navbarBO = navbarService.findById(id);
            if (navbarBO == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {
            navbarBO = new NavbarBO();
        }
        navbarBO.setDescription(form.getDescription());
        // Nếu là phần tử cha thì set level là 1 còn là con thì level là 2
        if (form.getParentId() != null) {
            navbarBO.setLevel(Constants.LEVEL.TWO);
        } else {
            navbarBO.setLevel(Constants.LEVEL.ONE);
        }
        navbarBO.setName(form.getName());
        navbarBO.setUrl(form.getUrl());
        navbarBO.setParentId(form.getParentId());
        navbarService.saveOrUpdate(navbarBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(navbarBO);
    }

    /**
     * delete
     *
     * @param navbarId
     * @return
     */
    @PreAuthorize("hasRole('Admin')")
    @DeleteMapping(path = "/{navbarId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long navbarId) {

        NavbarBO bo;
        if (navbarId > 0L) {
            bo = navbarService.findById(navbarId);
            if (bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            navbarService.delete(bo);
            return Response.success("Xóa thành công navbar " + bo.getName() + " với URL " + bo.getUrl(),Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }
}
