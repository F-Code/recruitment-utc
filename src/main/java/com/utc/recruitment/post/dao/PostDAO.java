package com.utc.recruitment.post.dao;

import com.utc.recruitment.post.bean.PostBean;
import com.utc.recruitment.post.bo.PostBO;
import com.utc.recruitment.post.form.PostForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Transactional
@Repository
public interface PostDAO extends CrudRepository<PostBO, Long> {
    /**
     * List all Content
     */
    List<PostBO> findAll();

    List<PostBO> findByIdPostCategory(Long idPostCategory);

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<PostBean> getDatatables(CommonService commonService, PostForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,content As content      ";
        sql += "       ,created_date As createdDate  ";
        sql += "       ,short_content As shortContent ";
        sql += "       ,status As status       ";
        sql += "       ,title As title        ";
        sql += "       ,id_content_category As idContentCategory ";
        sql += "       FROM contents ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");


        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getContent(), strCondition, paramList, "content");
        Mixin.filter(formData.getShortContent(), strCondition, paramList, "short_content");
        Mixin.filter(formData.getStatus(), strCondition, paramList, "status");
        Mixin.filter(formData.getTitle(), strCondition, paramList, "title");
        Mixin.filter(formData.getIdContentCategory(), strCondition, paramList, "id_content_category");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, PostBean.class, formData.getPage(), formData.getRecordPage());
    }
}
