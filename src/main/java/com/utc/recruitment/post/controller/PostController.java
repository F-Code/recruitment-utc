
package com.utc.recruitment.post.controller;

import com.utc.recruitment.post.bean.PostBean;
import com.utc.recruitment.post.bo.PostBO;
import com.utc.recruitment.post.form.PostForm;
import com.utc.recruitment.post.service.PostService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/api/post")
@PreAuthorize("hasAnyRole('Marketing', 'Admin', 'HR', 'HR Admin', 'Candidate', 'Visiter')")
public class PostController {

    @Autowired
    private PostService postService;

    /**
     * findById
     *
     * @param contentId
     * @return
     */
    @GetMapping(path = "/{contentId}")
    public @ResponseBody
    Response findById(@PathVariable Long contentId) {

        PostBO postBO = postService.findById(contentId);
        if (postBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(postBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @GetMapping(path = "/get-all")
    public @ResponseBody
    DataTableResults<PostBean> processSearch(PostForm form) {
        return postService.getDatatables(form);
    }

    /**
     * saveOrUpdate ContentBO
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody PostForm form) throws Exception {
        Long id = Mixin.NVL(form.getId());
        PostBO postBO;
        if (id > 0L) {

            postBO = postService.findById(id);
            if (postBO == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {
            postBO = new PostBO();
        }
        postBO.setContent(form.getContent());
        postBO.setShortContent(form.getShortContent());
        postBO.setStatus(form.getStatus());
        postBO.setTitle(form.getTitle());
        postBO.setIdPostCategory(form.getIdContentCategory());
        postService.saveOrUpdate(postBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(postBO);
    }

    /**
     * delete
     *
     * @param contentId
     * @return
     */
    @DeleteMapping(path = "/{contentId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long contentId) {

        PostBO bo;
        if (contentId > 0L) {
            bo = postService.findById(contentId);
            if (bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            postService.delete(bo);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }
}
