
package com.utc.recruitment.post.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * ContentBean for table "contents"
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostBean {

    private Long id;
    private String content;
    private Date createdDate;
    private String shortContent;
    private Integer status;
    private String title;
    private Long idPostCategory;

}
