package com.utc.recruitment.post.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * ContentForm class
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostForm {

    private Long id;
    private String content;
    private Date createdDate;
    private String shortContent;
    private Integer status;
    private String title;
    private Long idContentCategory;
    private Integer page;
    private Integer recordPage;
}
