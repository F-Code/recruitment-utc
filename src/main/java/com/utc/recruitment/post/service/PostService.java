package com.utc.recruitment.post.service;

import com.utc.recruitment.post.bean.PostBean;
import com.utc.recruitment.post.bo.PostBO;
import com.utc.recruitment.post.dao.PostDAO;
import com.utc.recruitment.post.form.PostForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
public class PostService {

    @Autowired
    private PostDAO postDAO;
    @Autowired
    private CommonService commonService;

    /**
     * findById
     *
     * @param id
     * @return
     */
    public PostBO findById(Long id) {
        return postDAO.findById(id).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param postForm
     * @return
     */
    public DataTableResults<PostBean> getDatatables(PostForm postForm) {
        return postDAO.getDatatables(commonService, postForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(PostBO entity) {
        postDAO.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(PostBO entity) {
        postDAO.delete(entity);
    }

    public List<PostBO> findByIdPostCategory(Long idPostCategory) {
        return postDAO.findByIdPostCategory(idPostCategory);
    }
}
