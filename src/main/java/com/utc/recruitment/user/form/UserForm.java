package com.utc.recruitment.user.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {
    private String username;

    private String password;

    // Họ
    private String firstName;

    // Tên
    private String lastName;

    // Email
    private String email;

    // Số điện thoại
    private String phoneNumber;

    // Loại Tài khoản
    private String codeRole;
}
