package com.utc.recruitment.user.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Form dùng cho việc login
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserForm {
    private String username;
    private String password;
}
