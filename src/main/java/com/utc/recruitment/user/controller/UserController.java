package com.utc.recruitment.user.controller;

import com.utc.recruitment.mail.MailService;
import com.utc.recruitment.user.bo.RoleBO;
import com.utc.recruitment.user.bo.UserBO;
import com.utc.recruitment.user.bo.UserRoleBO;
import com.utc.recruitment.user.form.ProfileUserForm;
import com.utc.recruitment.user.form.UserForm;
import com.utc.recruitment.user.service.RoleService;
import com.utc.recruitment.user.service.UserRoleService;
import com.utc.recruitment.user.service.UserService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@CrossOrigin(origins = "*")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private MailService mailService;

    @Autowired
    private RoleService roleService;

    @PreAuthorize("hasRole('Admin')")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserBO> listUser() {
        return userService.findAll();
    }

    // Lấy chi tiết user
    @PreAuthorize("hasRole('USER')")
    @PostMapping(path = "/user/profile")
    public UserBO getProfileUser(@RequestBody ProfileUserForm user) {
        return userService.findById(user.getId());
    }

    @PostMapping(path = "/signup")
    public Response saveUser(@RequestBody UserForm user) {
        // Validate email
        if (user.getEmail() != null && !user.getEmail().trim().equals("")) {
            String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
            Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(user.getEmail());
            if (!matcher.find()) {
                return Response.custom(Constants.RESPONSE_TYPE.ERROR, "Email " + user.getEmail() + " không hợp lệ");
            }

            // Kiểm tra tồn tại của email
            UserBO isExistUser = userService.findByEmail(user.getEmail());
            if (isExistUser != null) {
                return Response.error("Email " + user.getEmail() + " đã được đăng ký. Vui lòng thử lại với tài khoản khác");
            }
        }
        UserBO userBO = userService.save(user);
        mailService.sendEmail(user.getEmail(), "Chào bạn " + user.getFirstName() + " " + user.getLastName() + "," + "\n" +
                "\n" +
                "Tên tài khoản : " + userBO.getUsername() + "\n" +
                "\n" +
                "Mật khẩu: " + user.getPassword(), "Econet: Đăng ký tài khoàn thành công");
        // TODO: Update bản ghi vào user_roles

        // Lấy role_id theo code truyền vào
        RoleBO roleBO = roleService.findByCode(user.getCodeRole());
        UserRoleBO userRoleBO = new UserRoleBO();
        userRoleBO.setRoleId(roleBO.getId());

        // Lấy lại userBo => Lấy id bản ghi
        userBO = userService.findByEmail(user.getEmail());
        Long userId = userBO.getId();

        userRoleBO.setUserId(userId);
        userRoleService.saveOrUpdate(userRoleBO);

        return Response.success("Đăng ký account thành công").withData(userBO);
    }

}
