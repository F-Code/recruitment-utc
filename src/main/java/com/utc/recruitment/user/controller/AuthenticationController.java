package com.utc.recruitment.user.controller;

import com.utc.recruitment.user.bean.UserBean;
import com.utc.recruitment.user.form.LoginUserForm;
import com.utc.recruitment.config.TokenProvider;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public @ResponseBody
    Response login(@RequestBody LoginUserForm form) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        form.getUsername(),
                        form.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);

        UserBean userBean = new UserBean();
        userBean.setToken(token);
        userBean.setAccount(form.getUsername());

        return Response.success(Constants.RESPONSE_TYPE.SUCCESS).withData(userBean);
    }

}
