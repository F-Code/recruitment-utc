package com.utc.recruitment.user.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Thông tin trả về cho người dùng sau khi login
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserBean {
    // Tên tài khoản
    private String account;
    // Token đăng nhập của người dùng
    private String token;
}
