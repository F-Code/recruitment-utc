package com.utc.recruitment.user.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserBO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // Tài khoản đăng nhập
    @Column
    private String username;
    // Mật khẩu
    @Column
    @JsonIgnore
    private String password;
    // Họ
    @Column
    private String firstName;
    // Tên
    @Column
    private String lastName;
    // Email
    @Column
    private String email;
    // Ngày sinh
    private Date birthYear;
    // Số điện thoại
    private String phoneNumber;
    // Giới thiệu bản thân
    private String summary;
    // Ảnh đại diện
    @Column
    private String avatarPath;
    // Trạng thái  hoạt động 0-1
    @Column
    private boolean status;
    // Đường dẫn CV
    @Column
    private String cvPath;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES", joinColumns = {
            @JoinColumn(name = "USER_ID")}, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_ID")})
    private Set<RoleBO> roles;
}
