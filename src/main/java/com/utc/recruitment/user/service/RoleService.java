package com.utc.recruitment.user.service;

import com.utc.recruitment.user.bo.RoleBO;
import com.utc.recruitment.user.dao.RoleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Transactional
    public void saveOrUpdate(RoleBO entity) {
        roleDAO.save(entity);
    }

    /**
     * Hàm lấy bản ghi role theo code HR hoặc CANDICATE
     * @param code
     * @return
     */
    public RoleBO findByCode(String code) {
        return roleDAO.findByCode(code);
    }

}
