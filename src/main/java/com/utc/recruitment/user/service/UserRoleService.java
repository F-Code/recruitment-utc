package com.utc.recruitment.user.service;

import com.utc.recruitment.user.bo.UserRoleBO;
import com.utc.recruitment.user.dao.UserRoleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Transactional
    public void saveOrUpdate(UserRoleBO entity) {
        userRoleDAO.save(entity);
    }
}
