package com.utc.recruitment.user.service.impl;

import com.utc.recruitment.user.bo.UserBO;
import com.utc.recruitment.user.form.UserForm;
import com.utc.recruitment.user.dao.UserDAO;
import com.utc.recruitment.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserBO user = userDAO.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Tên tài khoản hoặc mật khẩu không chính xác");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(UserBO user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
    }

    public List<UserBO> findAll() {
        List<UserBO> list = new ArrayList<>();
        userDAO.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        userDAO.deleteById(id);
    }

    @Override
    public UserBO findOne(String username) {
        return userDAO.findByUsername(username);
    }

    @Override
    public UserBO findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public UserBO findById(Long id) {
        return userDAO.findById(id).get();
    }

    @Override
    public UserBO save(UserForm user) {
        UserBO newUser = new UserBO();
        String username = user.getEmail().substring(0, user.getEmail().indexOf('@'));
        newUser.setUsername(username);
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setPhoneNumber(user.getPhoneNumber());
        return userDAO.save(newUser);
    }
}
