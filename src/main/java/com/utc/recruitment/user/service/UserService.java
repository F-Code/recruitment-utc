package com.utc.recruitment.user.service;

import com.utc.recruitment.user.bo.UserBO;
import com.utc.recruitment.user.form.UserForm;

import java.util.List;

public interface UserService {

    UserBO save(UserForm user);
    List<UserBO> findAll();
    void delete(long id);
    UserBO findOne(String username);
    UserBO findByEmail(String email);
    UserBO findById(Long id);
}
