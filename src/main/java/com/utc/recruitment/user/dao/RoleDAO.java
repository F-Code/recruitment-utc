package com.utc.recruitment.user.dao;

import com.utc.recruitment.user.bo.RoleBO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDAO extends CrudRepository<RoleBO, Long> {
    RoleBO findByCode(String code);
}
