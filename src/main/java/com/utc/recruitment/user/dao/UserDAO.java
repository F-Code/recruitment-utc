package com.utc.recruitment.user.dao;

import com.utc.recruitment.user.bo.UserBO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends CrudRepository<UserBO, Long> {
    UserBO findByUsername(String username);
    UserBO findByEmail(String email);
}
