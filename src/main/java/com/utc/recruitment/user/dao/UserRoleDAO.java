package com.utc.recruitment.user.dao;

import com.utc.recruitment.user.bo.UserRoleBO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleDAO extends CrudRepository<UserRoleBO, Long> {
}
