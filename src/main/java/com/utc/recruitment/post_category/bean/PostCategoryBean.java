
package com.utc.recruitment.post_category.bean;

import com.utc.recruitment.post.bean.PostBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * ContentBean for table "contents"
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostCategoryBean {
    private Long id;
    private String name;
    private List<PostBean> listPost;
}
