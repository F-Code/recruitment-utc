
package com.utc.recruitment.post_category.controller;

import com.utc.recruitment.post.bean.PostBean;
import com.utc.recruitment.post.bo.PostBO;
import com.utc.recruitment.post.service.PostService;
import com.utc.recruitment.post_category.bean.PostCategoryBean;
import com.utc.recruitment.post_category.bo.PostCategoryBO;
import com.utc.recruitment.post_category.form.PostCategoryForm;
import com.utc.recruitment.post_category.service.PostCategoryService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/api/post-categories")
public class PostCategoryController {

    @Autowired
    private PostCategoryService postCategoryService;

    @Autowired
    private PostService postService;

    /**
     * findById
     *
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Response findById(@PathVariable Long id) {
        PostCategoryBO postCategoryBO = postCategoryService.findById(id);
        if (postCategoryBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        PostCategoryBean postCategoryBean = new PostCategoryBean();
        postCategoryBean.setId(postCategoryBO.getId());
        postCategoryBean.setName(postCategoryBean.getName());
        List<PostBean> postBeans = new ArrayList<>();
        List<PostBO> postBOS = postService.findByIdPostCategory(id);
        for (PostBO bo : postBOS) {
            PostBean bean = new PostBean();
            BeanUtils.copyProperties(bean, bo);
            postBeans.add(bean);
        }
        // Set lại giá trị cho list
        postCategoryBean.setListPost(postBeans);
        return Response.success("Lấy chi tiết thành công").withData(postCategoryBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @GetMapping(path = "/get-all")
    public @ResponseBody
    DataTableResults<PostCategoryBean> processSearch(PostCategoryForm form) {
        return postCategoryService.getDatatables(form);
    }

    /**
     * saveOrUpdate ContentBO
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody PostCategoryForm form) throws Exception {
        Long id = Mixin.NVL(form.getId());
        PostCategoryBO postBO;
        if (id > 0L) {

            postBO = postCategoryService.findById(id);
            if (postBO == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {
            postBO = new PostCategoryBO();
        }
        postBO.setName(form.getName());
        postCategoryService.saveOrUpdate(postBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(postBO);
    }

    /**
     * Kiểm tra id có tồn tại không => Kiểm tra có bản ghi tin tức thuộc loại bài viết này không => thực hiện việc xóa
     *
     * @param id
     * @return
     */
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long id) {
        PostCategoryBO bo;
        if (id > 0L) {
            bo = postCategoryService.findById(id);
            if (bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            List<PostBO> isPostBOWithCategory = postService.findByIdPostCategory(bo.getId());
            if (isPostBOWithCategory.size() > 0) {
                return Response.custom("Vui lòng xóa danh sách tin tức thuộc loại " + bo.getName(), Constants.RESPONSE_CODE.ERROR);
            }
            postCategoryService.delete(bo);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }
}
