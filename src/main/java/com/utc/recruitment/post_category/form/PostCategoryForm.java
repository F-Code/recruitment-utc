package com.utc.recruitment.post_category.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PostCategoryForm class
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostCategoryForm {
    private Long id;
    private String name;
    private Integer page;
    private Integer recordPage;
}
