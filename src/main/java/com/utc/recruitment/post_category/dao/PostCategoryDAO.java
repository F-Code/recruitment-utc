package com.utc.recruitment.post_category.dao;

import com.utc.recruitment.post_category.bean.PostCategoryBean;
import com.utc.recruitment.post_category.bo.PostCategoryBO;
import com.utc.recruitment.post_category.form.PostCategoryForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Transactional
@Repository
public interface PostCategoryDAO extends CrudRepository<PostCategoryBO, Long> {
    /**
     * List all Content
     */
    List<PostCategoryBO> findAll();

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<PostCategoryBean> getDatatables(CommonService commonService, PostCategoryForm formData) {
        List<Object> paramList = new ArrayList<>();
        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,name As name      ";
        sql += "       FROM post_categories ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");
        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getName(), strCondition, paramList, "name");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, PostCategoryBean.class, formData.getPage(), formData.getRecordPage());
    }
}
