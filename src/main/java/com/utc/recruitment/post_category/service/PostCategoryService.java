package com.utc.recruitment.post_category.service;

import com.utc.recruitment.post_category.bean.PostCategoryBean;
import com.utc.recruitment.post_category.bo.PostCategoryBO;
import com.utc.recruitment.post_category.dao.PostCategoryDAO;
import com.utc.recruitment.post_category.form.PostCategoryForm;
import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PostCategoryService {

    @Autowired
    private PostCategoryDAO postCategoryDAO;
    @Autowired
    private CommonService commonService;

    /**
     * findById
     *
     * @param ContentId
     * @return
     */
    public PostCategoryBO findById(Long ContentId) {
        return postCategoryDAO.findById(ContentId).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param postCategoryForm
     * @return
     */
    public DataTableResults<PostCategoryBean> getDatatables(PostCategoryForm postCategoryForm) {
        return postCategoryDAO.getDatatables(commonService, postCategoryForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(PostCategoryBO entity) {
        postCategoryDAO.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(PostCategoryBO entity) {
        postCategoryDAO.delete(entity);
    }
}
