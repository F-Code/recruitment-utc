
package com.utc.recruitment.job.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * JobForm class
 *
 * @author
 * @since 1.0
 * @version 1.0
 */
public class JobWorkForm {

    private Long id;
    private String benefit;
    private String descriptionJob;
    private Date expirationDate;
    private Long idCompany;
    private Integer numberPeople;
    private String position;
    private String require;
    private Integer salaryFrom;
    private Integer salaryTo;
    private String title;
    private Integer typeJob;
    private String typeSalary;
    private Integer yearsOfExperience;
    private Integer page;
    private Integer recordPage;
}
