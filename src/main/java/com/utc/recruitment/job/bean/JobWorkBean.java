
package com.utc.recruitment.job.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * JobBean for table "jobs"
 *
 * @author
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobWorkBean {

    private Long id;
    private String benefit;
    private String descriptionJob;
    private Date expirationDate;
    private Long idCompany;
    private Integer numberPeople;
    private String position;
    private String require;
    private Integer salaryFrom;
    private Integer salaryTo;
    private String title;
    private Integer typeJob;
    private String typeSalary;
    private Integer yearsOfExperience;

}
