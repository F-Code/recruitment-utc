
package com.utc.recruitment.job.controller;

import com.utc.recruitment.job.bean.JobWorkBean;
import com.utc.recruitment.job.bo.JobBO;
import com.utc.recruitment.job.form.JobWorkForm;
import com.utc.recruitment.job.service.JobService;
import com.utc.recruitment.utils.Constants;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import com.utc.recruitment.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
@RequestMapping("/api/job")
@PreAuthorize("hasAnyRole('Marketing', 'Admin', 'HR', 'HR Admin', 'Candidate', 'Visiter')")
public class JobWorkController {

    @Autowired
    private JobService jobService;

    /**
     * findById
     *
     * @param jobId
     * @return
     */
    @GetMapping(path = "/{jobId}")
    public @ResponseBody
    Response findById(@PathVariable Long jobId) {

        JobBO jobBO = jobService.findById(jobId);
        if (jobBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(jobBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @GetMapping(path = "/get-all")
    public @ResponseBody
    DataTableResults<JobWorkBean> processSearch(JobWorkForm form) {

        return jobService.getDatatables(form);
    }

    /**
     * saveOrUpdate JobBO
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody JobWorkForm form) throws Exception {
        Long id = Mixin.NVL(form.getId());
        JobBO jobBO;
        if (id > 0L) {

            jobBO = jobService.findById(id);
            if (jobBO == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
        } else {

            jobBO = new JobBO();
        }
        jobBO.setBenefit(form.getBenefit());
        jobBO.setDescriptionJob(form.getDescriptionJob());
        jobBO.setExpirationDate(form.getExpirationDate());
        jobBO.setIdCompany(form.getIdCompany());
        jobBO.setNumberPeople(form.getNumberPeople());
        jobBO.setPosition(form.getPosition());
        jobBO.setRequire(form.getRequire());
        jobBO.setSalaryFrom(form.getSalaryFrom());
        jobBO.setSalaryTo(form.getSalaryTo());
        jobBO.setTitle(form.getTitle());
        jobBO.setTypeJob(form.getTypeJob());
        jobBO.setTypeSalary(form.getTypeSalary());
        jobBO.setYearsOfExperience(form.getYearsOfExperience());
        jobService.saveOrUpdate(jobBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(jobBO);
    }

    /**
     * delete
     *
     * @param jobId
     * @return
     */
    @DeleteMapping(path = "/{jobId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long jobId) {

        JobBO bo;
        if (jobId > 0L) {
            bo = jobService.findById(jobId);
            if (bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            jobService.delete(bo);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS);
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }
}
