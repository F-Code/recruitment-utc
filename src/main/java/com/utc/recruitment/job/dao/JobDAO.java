

package com.utc.recruitment.job.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import com.utc.recruitment.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.utc.recruitment.job.bean.JobWorkBean;
import com.utc.recruitment.job.form.JobWorkForm;
import com.utc.recruitment.job.bo.JobBO;


@Transactional
@Repository
public interface JobDAO extends CrudRepository<JobBO, Long>
{
    /**
     * List all Job
     */
    public List<JobBO> findAll();

    /**
     * get data by datatable
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<JobWorkBean> getDatatables(CommonService commonService, JobWorkForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,benefit As benefit      ";
        sql += "       ,descriptionJob As descriptionJob ";
        sql += "       ,expirationDate As expirationDate ";
        sql += "       ,idCompany As idCompany    ";
        sql += "       ,numberPeople As numberPeople ";
        sql += "       ,position As position     ";
        sql += "       ,require As require      ";
        sql += "       ,salaryFrom As salaryFrom   ";
        sql += "       ,salaryTo As salaryTo     ";
        sql += "       ,title As title        ";
        sql += "       ,typeJob As typeJob      ";
        sql += "       ,typeSalary As typeSalary   ";
        sql += "       ,yearsOfExperience As yearsOfExperience ";
        sql += "       FROM jobs ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");


        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getBenefit(), strCondition, paramList, "benefit");
        Mixin.filter(formData.getDescriptionJob(), strCondition, paramList, "description_job");
        Mixin.filter(formData.getExpirationDate(), strCondition, paramList, "expiration_date");
        Mixin.filter(formData.getIdCompany(), strCondition, paramList, "id_company");
        Mixin.filter(formData.getNumberPeople(), strCondition, paramList, "number_people");
        Mixin.filter(formData.getPosition(), strCondition, paramList, "position");
        Mixin.filter(formData.getRequire(), strCondition, paramList, "require");
        Mixin.filter(formData.getSalaryFrom(), strCondition, paramList, "salary_from");
        Mixin.filter(formData.getSalaryTo(), strCondition, paramList, "salary_to");
        Mixin.filter(formData.getTitle(), strCondition, paramList, "title");
        Mixin.filter(formData.getTypeJob(), strCondition, paramList, "type_job");
        Mixin.filter(formData.getTypeSalary(), strCondition, paramList, "type_salary");
        Mixin.filter(formData.getYearsOfExperience(), strCondition, paramList, "years_of_experience");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, JobWorkBean.class, formData.getPage(), formData.getRecordPage());
    }
}
