

package com.utc.recruitment.job.service;

import javax.transaction.Transactional;

import com.utc.recruitment.utils.CommonService;
import com.utc.recruitment.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.utc.recruitment.job.bean.JobWorkBean;
import com.utc.recruitment.job.form.JobWorkForm;
import com.utc.recruitment.job.bo.JobBO;
import com.utc.recruitment.job.dao.JobDAO;


@Service
public class JobService {

    @Autowired
    private JobDAO jobdao;

    @Autowired
    private CommonService commonService;

    /**
     * findById
     * @param jobId
     * @return
     */
    public JobBO findById(Long jobId) {
        return jobdao.findById(jobId).orElse(null);
    }

    /**
     * getDatatables
     * @param jobWorkForm
     * @return
     */
    public DataTableResults<JobWorkBean> getDatatables(JobWorkForm jobWorkForm) {
        return jobdao.getDatatables(commonService, jobWorkForm);
    }

    /**
     * saveOrUpdate
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(JobBO entity) {
        jobdao.save(entity);
    }

    /**
     * delete
     * @param entity
     */
    public void delete(JobBO entity) {
        jobdao.delete(entity);
    }
}
