

package com.utc.recruitment.job.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "jobs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobBO {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String benefit;

    @Column
    private String descriptionJob;

    @Column
    private Date expirationDate;

    @Column
    private Long idCompany;

    @Column
    private Integer numberPeople;

    @Column
    private String position;

    @Column
    private String require;

    @Column
    private Integer salaryFrom;

    @Column
    private Integer salaryTo;

    @Column
    private String title;

    @Column
    private Integer typeJob;

    @Column
    private String typeSalary;

    @Column
    private Integer yearsOfExperience;

}
