/*
 Navicat Premium Data Transfer

 Source Server         : Local-MSSQL
 Source Server Type    : SQL Server
 Source Server Version : 10504000
 Source Host           : localhost:1433
 Source Catalog        : UTC_RECRUITMENT
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 10504000
 File Encoding         : 65001

 Date: 03/05/2021 20:47:38
*/


-- ----------------------------
-- Table structure for contents
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[contents]') AND type IN ('U'))
	DROP TABLE [dbo].[contents]
GO

CREATE TABLE [dbo].[contents] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [created_date] datetime2(7) NULL,
  [id_content_category] bigint NULL,
  [short_content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [status] int NULL,
  [title] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[contents] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for delineations
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[delineations]') AND type IN ('U'))
	DROP TABLE [dbo].[delineations]
GO

CREATE TABLE [dbo].[delineations] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[delineations] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for descriptions
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[descriptions]') AND type IN ('U'))
	DROP TABLE [dbo].[descriptions]
GO

CREATE TABLE [dbo].[descriptions] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[descriptions] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for infomations
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[infomations]') AND type IN ('U'))
	DROP TABLE [dbo].[infomations]
GO

CREATE TABLE [dbo].[infomations] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [address] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [email] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [fax] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [level] int NULL,
  [logo_path] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [phone] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[infomations] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for jobs
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jobs]') AND type IN ('U'))
	DROP TABLE [dbo].[jobs]
GO

CREATE TABLE [dbo].[jobs] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [benefit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [description_job] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [expiration_date] datetime2(7) NULL,
  [id_company] bigint NULL,
  [number_people] int NULL,
  [position] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [require] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [salary_from] int NULL,
  [salary_to] int NULL,
  [title] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [type_job] int NULL,
  [type_salary] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [years_of_experience] int NULL
)
GO

ALTER TABLE [dbo].[jobs] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for navbars
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[navbars]') AND type IN ('U'))
	DROP TABLE [dbo].[navbars]
GO

CREATE TABLE [dbo].[navbars] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [description] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [level] int NULL,
  [name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [parent_id] bigint NULL,
  [url] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [type] int NULL
)
GO

ALTER TABLE [dbo].[navbars] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of [navbars]
-- ----------------------------
SET IDENTITY_INSERT [dbo].[navbars] ON
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'1', N'Tin tức', N'1', N'Tin tức', NULL, N'/content', N'1')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'2', N'Giới thiệu', N'1', N'Giới thiệu', NULL, N'/delineation', N'1')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'3', N'Tuyển dụng', N'1', N'Tuyển dụng', NULL, N'/job', N'1')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'4', N'Liên hệ', N'1', N'Liên hệ', NULL, N'/support', N'1')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'5', N'Quản lý người dùng', N'1', N'Quản lý người dùng', NULL, N'/user-management', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'6', N'Quản lý hỗ trợ khách hàng', N'1', N'Quản lý hỗ trợ khách hàng', NULL, N'/support-management', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'7', N'Quản lý công việc', N'1', N'Hồ sơ cá nhân', NULL, N'/job-management', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'8', N'Thống kê', N'1', N'Thống kê', NULL, N'/report-management', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'10', N'Quản lý CV ứng tuyển', N'1', N'Quản lý CV ứng tuyển', NULL, N'/cv-apply-management', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'11', N'Job đã hết hạn', N'2', N'Job đã hết hạn', N'10', N'/older-job', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'12', N'Job đang chạy', N'2', N'Job đang chạy', N'10', N'/process-job', N'0')
GO

INSERT INTO [dbo].[navbars] ([id], [description], [level], [name], [parent_id], [url], [type]) VALUES (N'13', N'Ứng viên tiềm năng', N'2', N'Ứng viên tiềm năng', N'10', N'/cv-reject', N'0')
GO

SET IDENTITY_INSERT [dbo].[navbars] OFF
GO


-- ----------------------------
-- Table structure for post_categories
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[post_categories]') AND type IN ('U'))
	DROP TABLE [dbo].[post_categories]
GO

CREATE TABLE [dbo].[post_categories] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[post_categories] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for posts
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[posts]') AND type IN ('U'))
	DROP TABLE [dbo].[posts]
GO

CREATE TABLE [dbo].[posts] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [created_date] datetime2(7) NULL,
  [id_post_category] bigint NULL,
  [short_content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [status] int NULL,
  [title] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[posts] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for roles
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[roles]') AND type IN ('U'))
	DROP TABLE [dbo].[roles]
GO

CREATE TABLE [dbo].[roles] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [description] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [code] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[roles] SET (LOCK_ESCALATION = AUTO)
GO


-- ----------------------------
-- Records of [roles]
-- ----------------------------
SET IDENTITY_INSERT [dbo].[roles] ON
GO

INSERT INTO [dbo].[roles] ([id], [description], [name], [code]) VALUES (N'7', N'Là người hỗ trợ hoạt động, bảo trì hệ thống và trực tiếp quản lý User', N'Admin', N'ADMIN')
GO

INSERT INTO [dbo].[roles] ([id], [description], [name], [code]) VALUES (N'8', N'Là người quản lý họat động của website, trực tiếp quản lý trang chủ, quản lý tin tức', N'Marketing', N'MAR')
GO

INSERT INTO [dbo].[roles] ([id], [description], [name], [code]) VALUES (N'9', N'Là người đăng tin tuyển dụng', N'HR', N'HR')
GO

INSERT INTO [dbo].[roles] ([id], [description], [name], [code]) VALUES (N'10', N'Là người kiểm duyệt tin trước khi đăng và kiểm duyệt trạng thái ứng tuyển của Candidate', N'HR Admin', N'HR_ADMIN')
GO

INSERT INTO [dbo].[roles] ([id], [description], [name], [code]) VALUES (N'11', N'Là người ứng tuyển', N'Candidate', N'CANDICATE')
GO

INSERT INTO [dbo].[roles] ([id], [description], [name], [code]) VALUES (N'12', N'Đây không phải là thành viên của hệ thống, không có tài khoản đăng nhập', N'Visiter', N'VISITER')
GO

SET IDENTITY_INSERT [dbo].[roles] OFF
GO


-- ----------------------------
-- Table structure for slides
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[slides]') AND type IN ('U'))
	DROP TABLE [dbo].[slides]
GO

CREATE TABLE [dbo].[slides] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [path] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [type] bigint NULL
)
GO

ALTER TABLE [dbo].[slides] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for supports
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[supports]') AND type IN ('U'))
	DROP TABLE [dbo].[supports]
GO

CREATE TABLE [dbo].[supports] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [content] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [email] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [phone_number] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [status] int NULL,
  [title] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[supports] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_roles]') AND type IN ('U'))
	DROP TABLE [dbo].[user_roles]
GO

CREATE TABLE [dbo].[user_roles] (
  [user_id] bigint NOT NULL,
  [role_id] bigint NOT NULL
)
GO

ALTER TABLE [dbo].[user_roles] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of [user_roles]
-- ----------------------------
INSERT INTO [dbo].[user_roles]  VALUES (N'1', N'7')
GO


-- ----------------------------
-- Table structure for users
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type IN ('U'))
	DROP TABLE [dbo].[users]
GO

CREATE TABLE [dbo].[users] (
  [id] bigint IDENTITY(1,1) NOT NULL,
  [avatar_path] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [birth_year] datetime2(7) NULL,
  [cv_path] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [email] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [first_name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [last_name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [password] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [phone_number] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [status] bit NULL,
  [summary] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [username] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [dbo].[users] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of [users]
-- ----------------------------
SET IDENTITY_INSERT [dbo].[users] ON
GO

INSERT INTO [dbo].[users] ([id], [avatar_path], [birth_year], [cv_path], [email], [first_name], [last_name], [password], [phone_number], [status], [summary], [username]) VALUES (N'1', NULL, NULL, NULL, NULL, NULL, NULL, N'$2a$10$zcRvHYOZ28DpumuZY1SA4Ob5DFJ3ERNT78tMIGBCVamE/O3vAG2Gy', NULL, N'0', NULL, N'admin')
GO

SET IDENTITY_INSERT [dbo].[users] OFF
GO


-- ----------------------------
-- Procedure structure for sp_upgraddiagrams
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_upgraddiagrams]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_upgraddiagrams]
GO

CREATE PROCEDURE [dbo].[sp_upgraddiagrams]
	AS
	BEGIN
		IF OBJECT_ID(N'dbo.sysdiagrams') IS NOT NULL
			return 0;
	
		CREATE TABLE dbo.sysdiagrams
		(
			name sysname NOT NULL,
			principal_id int NOT NULL,	-- we may change it to varbinary(85)
			diagram_id int PRIMARY KEY IDENTITY,
			version int,
	
			definition varbinary(max)
			CONSTRAINT UK_principal_name UNIQUE
			(
				principal_id,
				name
			)
		);


		/* Add this if we need to have some form of extended properties for diagrams */
		/*
		IF OBJECT_ID(N'dbo.sysdiagram_properties') IS NULL
		BEGIN
			CREATE TABLE dbo.sysdiagram_properties
			(
				diagram_id int,
				name sysname,
				value varbinary(max) NOT NULL
			)
		END
		*/

		IF OBJECT_ID(N'dbo.dtproperties') IS NOT NULL
		begin
			insert into dbo.sysdiagrams
			(
				[name],
				[principal_id],
				[version],
				[definition]
			)
			select	 
				convert(sysname, dgnm.[uvalue]),
				DATABASE_PRINCIPAL_ID(N'dbo'),			-- will change to the sid of sa
				0,							-- zero for old format, dgdef.[version],
				dgdef.[lvalue]
			from dbo.[dtproperties] dgnm
				inner join dbo.[dtproperties] dggd on dggd.[property] = 'DtgSchemaGUID' and dggd.[objectid] = dgnm.[objectid]	
				inner join dbo.[dtproperties] dgdef on dgdef.[property] = 'DtgSchemaDATA' and dgdef.[objectid] = dgnm.[objectid]
				
			where dgnm.[property] = 'DtgSchemaNAME' and dggd.[uvalue] like N'_EA3E6268-D998-11CE-9454-00AA00A3F36E_' 
			return 2;
		end
		return 1;
	END
GO


-- ----------------------------
-- Procedure structure for sp_helpdiagrams
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_helpdiagrams]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_helpdiagrams]
GO

CREATE PROCEDURE [dbo].[sp_helpdiagrams]
	(
		@diagramname sysname = NULL,
		@owner_id int = NULL
	)
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		DECLARE @user sysname
		DECLARE @dboLogin bit
		EXECUTE AS CALLER;
			SET @user = USER_NAME();
			SET @dboLogin = CONVERT(bit,IS_MEMBER('db_owner'));
		REVERT;
		SELECT
			[Database] = DB_NAME(),
			[Name] = name,
			[ID] = diagram_id,
			[Owner] = USER_NAME(principal_id),
			[OwnerID] = principal_id
		FROM
			sysdiagrams
		WHERE
			(@dboLogin = 1 OR USER_NAME(principal_id) = @user) AND
			(@diagramname IS NULL OR name = @diagramname) AND
			(@owner_id IS NULL OR principal_id = @owner_id)
		ORDER BY
			4, 5, 1
	END
GO


-- ----------------------------
-- Procedure structure for sp_helpdiagramdefinition
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_helpdiagramdefinition]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_helpdiagramdefinition]
GO

CREATE PROCEDURE [dbo].[sp_helpdiagramdefinition]
	(
		@diagramname 	sysname,
		@owner_id	int	= null 		
	)
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		set nocount on

		declare @theId 		int
		declare @IsDbo 		int
		declare @DiagId		int
		declare @UIDFound	int
	
		if(@diagramname is null)
		begin
			RAISERROR (N'E_INVALIDARG', 16, 1);
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner');
		if(@owner_id is null)
			select @owner_id = @theId;
		revert; 
	
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname;
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId ))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1);
			return -3
		end

		select version, definition FROM dbo.sysdiagrams where diagram_id = @DiagId ; 
		return 0
	END
GO


-- ----------------------------
-- Procedure structure for sp_creatediagram
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_creatediagram]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_creatediagram]
GO

CREATE PROCEDURE [dbo].[sp_creatediagram]
	(
		@diagramname 	sysname,
		@owner_id		int	= null, 	
		@version 		int,
		@definition 	varbinary(max)
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
	
		declare @theId int
		declare @retval int
		declare @IsDbo	int
		declare @userName sysname
		if(@version is null or @diagramname is null)
		begin
			RAISERROR (N'E_INVALIDARG', 16, 1);
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID(); 
		select @IsDbo = IS_MEMBER(N'db_owner');
		revert; 
		
		if @owner_id is null
		begin
			select @owner_id = @theId;
		end
		else
		begin
			if @theId <> @owner_id
			begin
				if @IsDbo = 0
				begin
					RAISERROR (N'E_INVALIDARG', 16, 1);
					return -1
				end
				select @theId = @owner_id
			end
		end
		-- next 2 line only for test, will be removed after define name unique
		if EXISTS(select diagram_id from dbo.sysdiagrams where principal_id = @theId and name = @diagramname)
		begin
			RAISERROR ('The name is already used.', 16, 1);
			return -2
		end
	
		insert into dbo.sysdiagrams(name, principal_id , version, definition)
				VALUES(@diagramname, @theId, @version, @definition) ;
		
		select @retval = @@IDENTITY 
		return @retval
	END
GO


-- ----------------------------
-- Procedure structure for sp_renamediagram
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_renamediagram]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_renamediagram]
GO

CREATE PROCEDURE [dbo].[sp_renamediagram]
	(
		@diagramname 		sysname,
		@owner_id		int	= null,
		@new_diagramname	sysname
	
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
		declare @theId 			int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
		declare @DiagIdTarg		int
		declare @u_name			sysname
		if((@diagramname is null) or (@new_diagramname is null))
		begin
			RAISERROR ('Invalid value', 16, 1);
			return -1
		end
	
		EXECUTE AS CALLER;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		REVERT;
	
		select @u_name = USER_NAME(@owner_id)
	
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1)
			return -3
		end
	
		-- if((@u_name is not null) and (@new_diagramname = @diagramname))	-- nothing will change
		--	return 0;
	
		if(@u_name is null)
			select @DiagIdTarg = diagram_id from dbo.sysdiagrams where principal_id = @theId and name = @new_diagramname
		else
			select @DiagIdTarg = diagram_id from dbo.sysdiagrams where principal_id = @owner_id and name = @new_diagramname
	
		if((@DiagIdTarg is not null) and  @DiagId <> @DiagIdTarg)
		begin
			RAISERROR ('The name is already used.', 16, 1);
			return -2
		end		
	
		if(@u_name is null)
			update dbo.sysdiagrams set [name] = @new_diagramname, principal_id = @theId where diagram_id = @DiagId
		else
			update dbo.sysdiagrams set [name] = @new_diagramname where diagram_id = @DiagId
		return 0
	END
GO


-- ----------------------------
-- Procedure structure for sp_alterdiagram
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_alterdiagram]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_alterdiagram]
GO

CREATE PROCEDURE [dbo].[sp_alterdiagram]
	(
		@diagramname 	sysname,
		@owner_id	int	= null,
		@version 	int,
		@definition 	varbinary(max)
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
	
		declare @theId 			int
		declare @retval 		int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
		declare @ShouldChangeUID	int
	
		if(@diagramname is null)
		begin
			RAISERROR ('Invalid ARG', 16, 1)
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID();	 
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		revert;
	
		select @ShouldChangeUID = 0
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		
		if(@DiagId IS NULL or (@IsDbo = 0 and @theId <> @UIDFound))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1);
			return -3
		end
	
		if(@IsDbo <> 0)
		begin
			if(@UIDFound is null or USER_NAME(@UIDFound) is null) -- invalid principal_id
			begin
				select @ShouldChangeUID = 1 ;
			end
		end

		-- update dds data			
		update dbo.sysdiagrams set definition = @definition where diagram_id = @DiagId ;

		-- change owner
		if(@ShouldChangeUID = 1)
			update dbo.sysdiagrams set principal_id = @theId where diagram_id = @DiagId ;

		-- update dds version
		if(@version is not null)
			update dbo.sysdiagrams set version = @version where diagram_id = @DiagId ;

		return 0
	END
GO


-- ----------------------------
-- Procedure structure for sp_dropdiagram
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_dropdiagram]') AND type IN ('P', 'PC', 'RF', 'X'))
	DROP PROCEDURE[dbo].[sp_dropdiagram]
GO

CREATE PROCEDURE [dbo].[sp_dropdiagram]
	(
		@diagramname 	sysname,
		@owner_id	int	= null
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
		declare @theId 			int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
	
		if(@diagramname is null)
		begin
			RAISERROR ('Invalid value', 16, 1);
			return -1
		end
	
		EXECUTE AS CALLER;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		REVERT; 
		
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1)
			return -3
		end
	
		delete from dbo.sysdiagrams where diagram_id = @DiagId;
	
		return 0;
	END
GO


-- ----------------------------
-- Function structure for fn_diagramobjects
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_diagramobjects]') AND type IN ('FN', 'FS', 'FT', 'IF', 'TF'))
	DROP FUNCTION[dbo].[fn_diagramobjects]
GO

CREATE FUNCTION [dbo].[fn_diagramobjects]() 
	RETURNS int
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		declare @id_upgraddiagrams		int
		declare @id_sysdiagrams			int
		declare @id_helpdiagrams		int
		declare @id_helpdiagramdefinition	int
		declare @id_creatediagram	int
		declare @id_renamediagram	int
		declare @id_alterdiagram 	int 
		declare @id_dropdiagram		int
		declare @InstalledObjects	int

		select @InstalledObjects = 0

		select 	@id_upgraddiagrams = object_id(N'dbo.sp_upgraddiagrams'),
			@id_sysdiagrams = object_id(N'dbo.sysdiagrams'),
			@id_helpdiagrams = object_id(N'dbo.sp_helpdiagrams'),
			@id_helpdiagramdefinition = object_id(N'dbo.sp_helpdiagramdefinition'),
			@id_creatediagram = object_id(N'dbo.sp_creatediagram'),
			@id_renamediagram = object_id(N'dbo.sp_renamediagram'),
			@id_alterdiagram = object_id(N'dbo.sp_alterdiagram'), 
			@id_dropdiagram = object_id(N'dbo.sp_dropdiagram')

		if @id_upgraddiagrams is not null
			select @InstalledObjects = @InstalledObjects + 1
		if @id_sysdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 2
		if @id_helpdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 4
		if @id_helpdiagramdefinition is not null
			select @InstalledObjects = @InstalledObjects + 8
		if @id_creatediagram is not null
			select @InstalledObjects = @InstalledObjects + 16
		if @id_renamediagram is not null
			select @InstalledObjects = @InstalledObjects + 32
		if @id_alterdiagram  is not null
			select @InstalledObjects = @InstalledObjects + 64
		if @id_dropdiagram is not null
			select @InstalledObjects = @InstalledObjects + 128
		
		return @InstalledObjects 
	END
GO


-- ----------------------------
-- Primary Key structure for table contents
-- ----------------------------
ALTER TABLE [dbo].[contents] ADD CONSTRAINT [PK__contents__3213E83F5D95E53A] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table delineations
-- ----------------------------
ALTER TABLE [dbo].[delineations] ADD CONSTRAINT [PK__delineat__3213E83F0D44F85C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table descriptions
-- ----------------------------
ALTER TABLE [dbo].[descriptions] ADD CONSTRAINT [PK__descript__3213E83F65370702] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table infomations
-- ----------------------------
ALTER TABLE [dbo].[infomations] ADD CONSTRAINT [PK__infomati__3213E83F11158940] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table jobs
-- ----------------------------
ALTER TABLE [dbo].[jobs] ADD CONSTRAINT [PK__jobs__3213E83F6CD828CA] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table navbars
-- ----------------------------
ALTER TABLE [dbo].[navbars] ADD CONSTRAINT [PK__navbars__3213E83F14E61A24] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table post_categories
-- ----------------------------
ALTER TABLE [dbo].[post_categories] ADD CONSTRAINT [PK__post_cat__3213E83F29E1370A] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table posts
-- ----------------------------
ALTER TABLE [dbo].[posts] ADD CONSTRAINT [PK__posts__3213E83F18B6AB08] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE [dbo].[roles] ADD CONSTRAINT [PK__roles__3213E83F1C873BEC] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table slides
-- ----------------------------
ALTER TABLE [dbo].[slides] ADD CONSTRAINT [PK__slides__3213E83F7C1A6C5A] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table supports
-- ----------------------------
ALTER TABLE [dbo].[supports] ADD CONSTRAINT [PK__supports__3213E83F7FEAFD3E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user_roles
-- ----------------------------
ALTER TABLE [dbo].[user_roles] ADD CONSTRAINT [PK__user_rol__6EDEA1532057CCD0] PRIMARY KEY CLUSTERED ([user_id], [role_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE [dbo].[users] ADD CONSTRAINT [PK__users__3213E83F24285DB4] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table user_roles
-- ----------------------------
ALTER TABLE [dbo].[user_roles] ADD CONSTRAINT [FKh8ciramu9cc9q3qcqiv4ue8a6] FOREIGN KEY ([role_id]) REFERENCES [roles] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[user_roles] ADD CONSTRAINT [FKhfh9dx7w3ubf1co1vdev94g3f] FOREIGN KEY ([user_id]) REFERENCES [users] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

